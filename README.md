#Touch manipulator

Simple batch script and powershell script to turn on and off touch functionality on X1 Yoga 5th gen (and possibly other devices).

This is desirable when using pen because windows does not filter all of the touches when using it.

It also prolongs the battery life.


The batch file is an entry point, but it has to be run as administrator, which is tiring, when one would like to run the app from taskbar with pen.

To overcome this, it is nescessary to create a *scheduled task* and run that with shotcut using command:

schtasks /RUN /TN \Path\To\Scheduled_task
