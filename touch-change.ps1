echo 'Getting touch screen status'
$status = (Get-PnpDevice | Where-Object {$_.FriendlyName -like '*touch screen*'}).Status[0]
if ($status -eq 'O') {
	echo 'Touch screen currently enabled'
	echo 'Touch screen will be disabled'
	Get-PnpDevice | Where-Object {$_.FriendlyName -like '*touch screen*'} | Disable-PnpDevice -Confirm:$false
	echo 'Disabled successfully!'
	Start-Sleep -Seconds 1
} elseif ($status -eq 'E') {
	echo 'Touch screen currently disabled'
	echo 'Touch screen will be enabled'
	Get-PnpDevice | Where-Object {$_.FriendlyName -like '*touch screen*'} | Enable-PnpDevice -Confirm:$false
	echo 'Enabled successfully!'
	Start-Sleep -Seconds 1
} else {
	echo 'Unknown device state!'
	Start-Sleep -Seconds 10
}